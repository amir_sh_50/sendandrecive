import QtQuick 2.9
import QtQuick.Controls 2.2
import clientClass 1.0
ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Scroll")
    ClientClass{
        id:myclass_id
        onAverageChanged: averageF.text = myclass_id.average
    }
    Button{
        id: buttonsend_id
        onClicked:{myclass_id.cppInt = marknumF.text
 myclass_id.sendDatagram()
}
        Text {
           anchors.centerIn:parent.Center
           text: qsTr("send & answer")
           anchors.top: parent.verticalCenter
           anchors.right: parent.right
           anchors.bottom: parent.bottom
           anchors.left: parent.left
           anchors.topMargin: -13
           font.pointSize: 15
           font.bold: true
            font.family: "Tahoma"
        }

        anchors.right: parent.right
        anchors.rightMargin: 68
        anchors.left: parent.left
        anchors.leftMargin: 428
        anchors.topMargin: 217
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 217
        anchors.top: parent.top

    }

Text {
    id: text1
    x: 73
    y: 227
    width: 238
    height: 27
    text: qsTr("press button to get answer")
    font.family: "Tahoma"
    font.pointSize: 14
    elide: Text.ElideMiddle
}

TextField {
    id: averageF
    placeholderText: "average Is:"
    anchors.top: parent.top
    anchors.topMargin: 292
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 145
    anchors.left: parent.left
    anchors.leftMargin: 260
    anchors.right: parent.right
    anchors.rightMargin: 256
}

TextField {
    id: marknumF
    anchors.top: parent.top
    anchors.topMargin: 143
    anchors.bottom: parent.bottom
    anchors.bottomMargin: 294
    anchors.left: parent.left
    anchors.leftMargin: 260
    anchors.right: parent.right
    anchors.rightMargin: 243
    placeholderText: "enter the marks"
}
}
