#ifndef CLIENTCLASS_H
#define CLIENTCLASS_H

#include <QObject>
#include <QObject>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <iostream>
#include <QObject>
#include <QUdpSocket>
#include <QtNetwork>
class clientClass : public QObject
{
    Q_OBJECT
public:
    explicit clientClass(QObject *parent = nullptr);
    Q_PROPERTY(qint16 cppInt READ getcppInt() WRITE setCppInt() NOTIFY cppIntChanged())
    Q_PROPERTY(qint16 average READ getaverage WRITE setaverage NOTIFY averageChanged)
private:
qint16 m_cppint;
QUdpSocket *udpsocket = nullptr;
QHostAddress address = QHostAddress("192.168.10.11");
QJsonObject mark;
QByteArray markbyte;
QNetworkDatagram datagram;
QByteArray avebyte;
QJsonDocument doc;
QJsonObject aveobj;
QJsonValue avevalue;
qint16 m_average;
signals:
void cppIntChanged();
void averageChanged();
public slots:
    qint16 getcppInt();
    qint16 getaverage();
    void setCppInt(qint16 &a);
    void setaverage(qint16 &b);
    void sendDatagram();
    void recievedatagram();
};

#endif // CLIENTCLASS_H
